__author__ = 'cheng109'


### splite the jobs into one for each chip.

import subprocess
import sys
def readin(runFile):
    ifile = open(runFile, 'r')
    header="\n"
    chipList = None
    for line in ifile.readlines():
        temp = line.split()
        if line[0]=='#':
            header = header + line
        elif temp and line[0]!='#':
            command = line.split("\n")[0]

        if temp and temp[0]=="#chipList":
            chipList = temp[1].split("|")
    return header, chipList, command


def submitJobs(header, chipList, command):
    for chip in chipList:
        temp_script = open("run_"+chip, 'w')
        temp_script.write(header + command +" -s " + chip)
        temp_script.close()


    return None

if __name__=="__main__":
    initialFile = sys.argv[1]
    header, chipList,command = readin(initialFile)
    submitJobs(header, chipList, command)

